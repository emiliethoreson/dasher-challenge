import React from "react";
import Countdown from 'react-countdown';


function GameStatus(props) {
      const renderDisplay = () => {
        let display;
        switch(props.gameState) {
          case 'won':
            display = (<div className="alibi-container">
              <h4>BOWL</h4>
            </div>)
            break;
          case 'failed':
            display = (<div><h4>TRY<br />AGAIN</h4><button className="restartButton" onClick={() => window.location.reload(false)}>RESTART</button></div>)
            break;
          case 'playing':
            display = <div><Countdown
            controlled={false}
            date={props.startTime + 78000}
            onComplete={props.setFail} 
            zeroPadDays={0}
          />
          <h6>Find #<br /><span id="highlight" className={props.currentNum % 2 === 0 ? "even" : "odd"}>
              {props.currentNum}
            </span></h6>
          </div>
            break;
          default: display = <></>
        }
        return display;
      }

    return (
        <div className="instructions">
            {renderDisplay()}
        </div>
    )
}

export default GameStatus;
