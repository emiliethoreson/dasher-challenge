import React, { useState } from 'react';
import './App.css';

export default function App() {
  const [guess, setGuess] = useState([0, 0, 0, 0]);
  const [view, setView] = useState('input');

  const checkGuess = (e) => {
    let updatedGuess = guess;
    if (e.target.value.length > 1) e.target.value = e.target.value.slice(0, 1);
    updatedGuess[e.target.name] = e.target.value;
    setGuess(updatedGuess);
    if (guess.join('') === '7653') {
      window.location.href = "https://www.hoafromhell.co/cphb06";
    }
  }

  const checkPassword = (e) => {
    if (e.target.value === 'railroad') {
      setView('square');
    }
  }

  const handleFocus = (e) => {e.target.select();}

  function OpeningInput() {
    return (
      <div className="skeletonWrapper">
      <div className="inputWrapper">
        <input autoFocus className="input" name="0" onChange={checkGuess} type="number" inputMode='numeric' pattern="[0-9]*" min="0" max="9" onFocus={handleFocus}></input>
        <input className="input" name="1" onChange={checkGuess} type="number" inputMode='numeric' pattern="[0-9]*" min="0" max="9" onFocus={handleFocus}></input>
        <input className="input" name="2" onChange={checkGuess} type="number" inputMode='numeric' pattern="[0-9]*" min="0" max="9" onFocus={handleFocus}></input>
        <input className="input" name="3" onChange={checkGuess} type="number" inputMode='numeric' pattern="[0-9]*" min="0" max="9" onFocus={handleFocus}></input>
        </div>
        <div className="inputWrapper">
          <label className="label">or</label>
          <label className='label'>PASSCODE:</label>
          <input className="codewordinput" onChange={checkPassword} onFocus={handleFocus}></input>
        </div>

      </div>
    )
  }

  function SquareNarrative() {
    return (
      <div className="answerWrapper">
      <span className="text bold">... .- -<br />--- -- .--. . - - .-</span>
        <span className="text">
my darling child,<br />
Role as head of the house now, so<br />
Need you to be strong. Someday y<br />
Great force in this world - reme<br />
Greece vacation, instead of what<br />
Poor. The t.e.r. raids were politi<br />
Rue the day. - papa
</span>
      </div>
    )
  }

  const updateView = (view) => {
    switch(view) {
      case 'input': return <OpeningInput />;
      case 'square': return <SquareNarrative />;
      default: return <OpeningInput />;
    }
  }

  return (
    <div className="container">
      {updateView(view)}
    </div>
  );
}
